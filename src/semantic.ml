open Ast
open Sast

type symbol_table = {
	vars : Sast.var_decl list;
	funcs : Sast.func_decl list;
}

type env = {
	scope : symbol_table;
	nodes : node_decl list;
	curr_node : Sast.node_decl;
	in_func : bool;
	curr_ret_type : Ast.dtype;
}

let rec find_var env name = 
	try
		List.find (fun (var_name, _) -> var_name = name) env.scope.vars
	with Not_found ->
		raise (Failure("Variable " ^ name ^ " not in scope"))

let rec find_func env name = 
	try 
		List.find (fun f -> f.func_name = name) env.curr_node.helper_funcs
	with Not_found ->
		raise (Failure("function " ^ name ^ " not defined"))

let rec find_node env name = 
	try 
		List.find (fun node -> node.node_name = name) env.nodes
	with Not_found -> 
		raise (Failure("node " ^ name ^ " not defined"))

let same_type t arg = 
	let (_, arg_type) = arg in
		if t = arg_type then
			arg
		else 
			raise (Failure("mismatched types"))

let rec expr env = function
	| Ast.Noexpr -> Sast.Noexpr, VoidType
	| Ast.CharLiteral(lit) -> Sast.CharLit(lit), CharType
	| Ast.StringLiteral(lit) -> Sast.StringLit(lit), StringType
	| Ast.IntLiteral(lit) -> Sast.IntLit(lit), IntType
	| Ast.FloatLiteral(lit) -> Sast.FloatLit(lit), FloatType
	| Ast.BoolLiteral(lit) -> Sast.BoolLit(lit), BoolType
	| Ast.Id(id) ->
		let vdecl = 
			try
				find_var env id
			with Not_found ->
					raise (Failure ("undeclared identifier: " ^ id))
		in 
			let (name, typ) = vdecl in
				Sast.Id(name), typ
	| Ast.Binop (e1, op, e2) ->
		let e1 = expr env e1
		and e2 = expr env e2 in
			let (_, t1) = e1
			and (_, t2) = e2 in
				let t = 
					if (not (t1 = t2)) then
						raise (Failure ("type mismatch"))
					else match op with 
						| Ast.Add -> (match t1 with
							| IntType | FloatType | StringType -> t1
							| _ -> raise (Failure ("Type failure")))
						| Ast.Sub | Ast.Mult | Ast.Div | Ast.Mod -> (match t1 with
							| IntType | FloatType -> t1
							| _ -> raise (Failure ("Type failure")))
						| Ast.Eq | Ast.Neq -> (match t1 with
							| IntType | FloatType | CharType 
							| StringType | BoolType -> BoolType
							| _ -> raise (Failure ("Type failure")))
						| Ast.Lt | Ast.Gt | Ast.Leq | Ast.Geq -> (match t1 with
							| IntType | FloatType | CharType | StringType -> BoolType
							| _ -> raise (Failure ("Type failure")))
						| Ast.And | Ast.Or -> (match t1 with
							| BoolType -> BoolType
							| _ -> raise (Failure ("Mismatched types")))
				in Sast.Binop(e1, op, e2), t
	| Ast.Unop (op, e) ->
		let e = expr env e in
			let (_, t) = e in
				let t = match op with
					| Ast.Neg -> (match t with
						| IntType | FloatType -> t
						| _ -> raise (Failure ("Type failure")))
					| Ast.Not -> (match t with
						| BoolType -> BoolType
						| _ -> raise (Failure ("Type failure")))
				in Sast.Unop(op, e), t
	| Ast.Assign (id, e) ->
			let id = find_var env id
			and e = expr env e in
				let (name, t1) = id 
				and (_, t2) = e in
					if (t1 = t2) then 
						Sast.Assign(name, e), t1
					else 
						raise (Failure ("Type mismatch in assignment"))
	| Ast.Call (func_name, params) ->
			let args = List.map (fun s -> expr env s) params in
				let fdecl = find_func env func_name in
					let types = List.rev (List.map (fun (_, typ) -> typ) (List.rev fdecl.fparams)) in
						try
							Sast.Call(fdecl.func_name, List.map2 same_type types args), fdecl.ret_type
						with Invalid_argument(x) ->
							raise (Failure("Invalid number of arguments"))

let rec stmt env = function
	| Ast.Block(s1) ->
			let s1 = List.map (fun s -> stmt env s) s1 in
				Sast.Block(s1)
	| Ast.Expr(e) -> Sast.Expr(expr env e)
	| Ast.Return(e) -> 
		if (env.in_func = false) then
			raise (Failure("Return statment found in illegal context"))
		else 
			let (e, typ) = expr env e in
				if (typ = env.curr_ret_type) then	
					Sast.Return(e, typ)
				else
					raise (Failure("Return expression does not match return type of function"))
	| Ast.If(e, s1, s2) ->
		let e = expr env e in
			if ((snd e) = BoolType) then
				Sast.If(e, stmt env s1, stmt env s2)
			else 
				raise (Failure ("If condition must evaluate to a boolean"))
	| Ast.For (e1, e2, e3, s) ->
		let e2 = expr env e2 in
			if ((snd e2) = BoolType) then
				Sast.For (expr env e1, e2, expr env e3, stmt env s)
			else
				raise (Failure ("Continuation condition in For loop must be boolean"))
	| Ast.While (e, s) ->
		let e = expr env e in
			if ((snd e) = BoolType) then
				Sast.While (e, stmt env s)
			else
				raise (Failure ("While condition must be boolean"))
	| Ast.Print (e) -> 
		let e = expr env e 
		in Sast.Print(e)
	| Ast.Continue -> Sast.Continue
	| Ast.Break -> Sast.Break
	| Ast.Nostmt -> Sast.Nostmt
	| Ast.Forward(expr_list, name) ->
		let args = List.map (fun p -> expr env p) expr_list in
			let forward_node = find_node env name in
				let types = List.rev (List.map (fun (_, typ) -> typ) (List.rev forward_node.nparams)) in
					try
						Sast.Forward(forward_node.node_name, List.map2 same_type types args)
					with Invalid_argument(x) ->
						raise (Failure("Invalid number of arguments in forward"))

let node_exists nodes name = 
	List.exists (fun n -> n.node_name = name) nodes

let add_param env node param = 
			let Ast.Formal(typ, name) = param in
				let exists = List.exists (fun id -> (fst id) = name) node.nparams in
					if (exists) then
						raise (Failure("Redefinition of identifier in node parameters"))
					else
						let params = (name, typ) :: node.nparams
						in {
							node with nparams = params
						}

let add_local env node var = 
	let Ast.VarDecl(typ, name, e) = var in
		let exists = List.exists (fun v -> fst v = name) node.nlocals in
			if (exists) then
				raise (Failure ("Redefinition of variable " ^ name ^ " in node " ^ node.node_name))
			else
				let scope_vars = node.nparams @ node.nlocals in
					let scope = {
						env.scope with vars = scope_vars
					} in
						let env = {
							env with scope = scope
						} in
							let (_, t) = expr env e in
								if (t != typ) then 
									raise (Failure ("type error when initializing id " ^ name))
								else
									let var_list = (name, typ) :: node.nlocals
									in {
										node with nlocals = var_list
									}

let add_compute env node s =
	let stmts = s :: node.unchecked_body
	in {
		node with unchecked_body = stmts
	}

let add_func_param func param = 
	let Ast.Formal(typ, name) = param in
		let exists = List.exists (fun p -> name = fst p) func.fparams in
			if (exists) then
				raise (Failure("Redefinition of identifier in function parameters"))
			else
				let params = (name, typ) :: func.fparams
				in {
					func with fparams = params
				}


let add_func_local env node func var = 
	let Ast.VarDecl(typ, name, e) = var in
		let exists = List.exists (fun v -> name = fst v) func.flocals in
			if (exists) then
				raise (Failure("variable redefinition"))
			else
				let scope = {
					env.scope with vars = func.fparams @ func.flocals;
				} in
					let env = {
						env with scope = scope
					} in
						let (_, t) = expr env e in
							if (t != typ) then
								raise (Failure ("type error when initializing id " ^ name))
							else
								let var_list = (name, typ) :: func.flocals
								in {
									func with flocals = var_list
								}

let add_func_body func s =
	let stmts = s :: func.unchecked_fbody
	in {
		func with unchecked_fbody = stmts
	}

let add_func env node f = 
	let exists = List.exists (fun helpf -> f.fname = helpf.func_name) node.helper_funcs in
		if (exists) then
			raise (Failure("A function with name " ^ f.fname ^ " already exists in node " ^ node.node_name))
		else
			let func = {
				ret_type = f.return_type;
				func_name = f.fname;
				fparams = [];
				flocals = [];
				fbody = [];
				unchecked_fbody = [];
			} in
				let func = List.fold_left add_func_param func f.formals in
					let func = List.fold_left (add_func_local env node) func f.locals in
						let func = List.fold_left add_func_body func f.body in
							let funcs = func :: node.helper_funcs
							in {
								node with helper_funcs = funcs
							}

let add_node env node =
	if (node_exists env.nodes node.nname) then 
		raise (Failure ("Node with name " ^ node.nname ^ " already exists"))
	else
		let new_node = {
			node_name = node.nname;
			nparams = [];
			nlocals = [];
			nbody = [];
			unchecked_body = [];
			helper_funcs = [];
		} in
			let new_node = List.fold_left (add_param env) new_node (List.rev node.args) in
				let new_node = List.fold_left (add_local env) new_node (node.local_vars) in
					let new_node = List.fold_left (add_compute env) new_node (List.rev node.compute) in
						let new_node = List.fold_left (add_func env) new_node (List.rev node.functions) in
							let new_nodes = (new_node :: env.nodes)
							in {
								env with nodes = new_nodes
							}

let check_func_body env node func s = 
	let scope = {
		vars = func.fparams @ func.flocals;
		funcs = node.helper_funcs;
	} in
		let env = {
			env with scope = scope
		} in
			let s = stmt env s in
				let stmts = s :: func.fbody
				in {
					func with fbody = stmts
				}

let check_func env node func = 
	let new_func = {
		ret_type = func.ret_type;
		func_name = func.func_name;
		fparams = func.fparams;
		flocals = func.flocals;
		fbody = [];
		unchecked_fbody = func.unchecked_fbody;
	} in
		let env = {
			scope = env.scope;
			nodes = env.nodes;
			curr_node = env.curr_node;
			in_func = true;
			curr_ret_type = new_func.ret_type;
		} in
			let func = List.fold_left (check_func_body env node) new_func func.unchecked_fbody in
				let func_list = func :: node.helper_funcs
				in {
					node with helper_funcs = func_list
				}

let check_compute env node s =
	let scope = {
		vars = node.nparams @ node.nlocals;
		funcs = node.helper_funcs;
	} in
		let env = {
			env with scope = scope
		} in
			let s = stmt env s in
				let stmts = s :: node.nbody in
					let node = {
						node with nbody = stmts
					} in
						List.fold_left (check_func env) node node.helper_funcs

let check_node env node =
	let new_node = {
		node_name = node.node_name;
		nparams = node.nparams;
		nlocals = node.nlocals;
		nbody = [];
		unchecked_body = node.unchecked_body;
		helper_funcs = node.helper_funcs;
	} in
		let env = {
			env with curr_node = node
		} in
			let node = List.fold_left (check_compute env) new_node node.unchecked_body in
				let node_list = node :: env.nodes
				in {
					env with nodes = node_list
				}

let check_start_node node_list =
	try
		let _ = List.find (fun n -> n.node_name = "start") node_list
			in true
			(* Do we want to check params here? *)
	with Not_found ->
		false

let check program =
	let empty_scope = {
		vars = [];
		funcs = [];
	} 
	and empty_node = {
		node_name = "";
		nparams = [];
		nlocals = [];
		nbody = [];
		unchecked_body = [];
		helper_funcs = [];
	} in
		let global_env = {
			scope = empty_scope;
			nodes = [];
			curr_node = empty_node;
			in_func = false;
			curr_ret_type = VoidType;
		} in 
			let global_env = List.fold_left add_node global_env program in
				if (check_start_node global_env.nodes) then
					List.fold_left check_node global_env global_env.nodes
				else
					raise (Failure ("No start node found"))
